package com.example.uploadingfiles.entity;

import lombok.Data;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "public", name = "soal")
@DynamicUpdate
@Data
public class Soal extends BaseEntity {
    @Size(min = 5, max = 50)
    @Column(name = "matauji", length = 50)
    private String matauji;

    @Size(min = 0, max = 50)
    @Column(name = "penulis", length = 50)
    private String penulis;

    private String no;
    private String paket;
    private String kompetensi;
    private String indikator;
    private String materi;
    private String spesifikasi;
    private String level;
    private String text;

    @Lob
    @Column(name = "gambar")
    @Type(type="org.hibernate.type.BinaryType")
    private byte[] gambar;

    private String base64encodedimage;

    private String a;
    private String b;
    private String c;
    private String d;
    private String kunci;
    private String pembahasan;
}
