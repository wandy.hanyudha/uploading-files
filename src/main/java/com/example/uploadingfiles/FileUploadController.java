package com.example.uploadingfiles;

import com.example.uploadingfiles.dao.SoalDao;
import com.example.uploadingfiles.dto.ProsesImportDto;
import com.example.uploadingfiles.entity.Soal;
import com.example.uploadingfiles.service.AsyncService;
import com.example.uploadingfiles.service.UploadService;
import com.example.uploadingfiles.storage.StorageFileNotFoundException;
import com.example.uploadingfiles.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/")
public class FileUploadController {

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private UploadService uploadService;
    private final StorageService storageService;

    private ProsesImportDto prosesImportDto = new ProsesImportDto();

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    /*@GetMapping("/")
    public String listUploadFiles(Model model) throws IOException {
        model.addAttribute("files", storageService.loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                        "serveFile", path.getFileName().toString()).build().toUri().toString()
        ).collect(Collectors.toList()));

        return "uploadForm";
    }*/

    @GetMapping(value = {"/", "/list"})
    public String getList (ModelMap mm,
                           Model model,
                           @RequestParam(value = "search", required = false) String search,
                           @PageableDefault(size = 30) Pageable page) throws IOException {

        model.addAttribute("files", storageService.loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                        "serveFile", path.getFileName().toString()).build().toUri().toString()
        ).collect(Collectors.toList()));

        Page<Soal> result;

        if (search == null || search.isEmpty()) {
            result = uploadService.getAllAndPage(page);
        } else {
            result = uploadService.getByMateriAndPage(search, page);
            mm.addAttribute("search", search);
        }

        mm.addAttribute("data", result);

        return "uploadForm";
    }


    @GetMapping("/delete")
    public String delete(@RequestParam(value = "id", required = false) String id,
                         RedirectAttributes ra){
        Optional<Soal> o = uploadService.getById(id);
        if(o.isPresent()) {
            uploadService.delete(id);
        } else {
            ra.addFlashAttribute("message", "Data Soal tidak ditemukan");
        }
        return "redirect:/";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    /*
    @PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        storageService.store(file);
        redirectAttributes.addFlashAttribute("message", "You Successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:/";
    }
    */

    @PostMapping("/import")
    @ResponseBody
    public ResponseEntity<Void> doImportData (@RequestParam("file") MultipartFile file) {

        prosesImportDto = new ProsesImportDto();
        asyncService.doWork(uploadService.importFromWord(file, prosesImportDto));
        storageService.store(file);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }


}
