package com.example.uploadingfiles.dao;

import com.example.uploadingfiles.entity.Soal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoalDao extends JpaRepository<Soal, String> {
    Page<Soal>findAllByOrderByMatauji(Pageable page);

    Page<Soal>findAllByMateriContainingIgnoreCaseOrderByNoAsc(String name, Pageable page);
}
