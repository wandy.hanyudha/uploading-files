package com.example.uploadingfiles.service;

import com.example.uploadingfiles.dao.SoalDao;
import com.example.uploadingfiles.dto.ProsesImportDto;
import com.example.uploadingfiles.entity.Soal;
import com.example.uploadingfiles.storage.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.sl.usermodel.PictureData;
import org.apache.poi.sl.usermodel.SlideShow;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
//import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class WordGeneratorService {

    SlideShow slideShow = null;
    @Autowired
    private SoalDao soalDao;

    public void importUpload(MultipartFile file, ProsesImportDto prosesImportDto) throws Exception {

        XWPFDocument xdoc = new XWPFDocument(file.getInputStream());
        List<XWPFTable> table = xdoc.getTables();
        for (XWPFTable xwpfTable : table) {
            List<XWPFTableRow> row = xwpfTable.getRows();
            String matauji = xwpfTable.getRow(0).getCell(1).getText();
            String penulis = xwpfTable.getRow(0).getCell(3).getText();
            String no = xwpfTable.getRow(1).getCell(1).getText();
            String paket = xwpfTable.getRow(1).getCell(3).getText();
            String kompetensi = xwpfTable.getRow(2).getCell(1).getText();
            String indikator = xwpfTable.getRow(3).getCell(1).getText();
            String materi = xwpfTable.getRow(4).getCell(1).getText();
            String spesifikasi = xwpfTable.getRow(5).getCell(1).getText();
            String level = xwpfTable.getRow(6).getCell(1).getText();

            String teksaja = xwpfTable.getRow(7).getCell(1).getText();


            String a = xwpfTable.getRow(8).getCell(1).getText();
            String b = xwpfTable.getRow(9).getCell(1).getText();
            String c = xwpfTable.getRow(10).getCell(1).getText();
            String d = xwpfTable.getRow(11).getCell(1).getText();
            String kunci = xwpfTable.getRow(12).getCell(1).getText();
            String pembahasan = xwpfTable.getRow(13).getCell(1).getText();
            //System.out.println(xwpfTable.getRow(0).getCell(1).getText());
            //System.out.println(xwpfTable.getRow(7).getCell(1).getText());
            Soal soal;
            soal = new Soal();
            soal.setMatauji(matauji);
            soal.setPenulis(penulis);
            soal.setNo(no);
            soal.setPaket(paket);
            soal.setKompetensi(kompetensi);
            soal.setIndikator(indikator);
            soal.setMateri(materi);
            soal.setSpesifikasi(spesifikasi);
            soal.setLevel(level);

            soal.setText(teksaja);

            soal.setA(a);
            soal.setB(b);
            soal.setC(c);
            soal.setD(d);
            soal.setKunci(kunci);
            soal.setPembahasan(pembahasan);

            soalDao.save(soal);

            for (XWPFTableRow xwpfTableRow : row) {
                List<XWPFTableCell> cell = xwpfTableRow.getTableCells();
                for (XWPFTableCell xwpfTableCell : cell) {
                    if (xwpfTableCell != null) {
                        //System.out.println(xwpfTableCell.getText());
                        //System.out.println(xwpfTable.getRow(0).getCell(1).getText());
                        String s = xwpfTable.getRow(7).getCell(1).getText();
                        for (XWPFParagraph p : xwpfTableCell.getParagraphs()) {
                            for (XWPFRun run : p.getRuns()) {
                                for (XWPFPicture pic : run.getEmbeddedPictures()) {
                                    byte[] pictureData = pic.getPictureData().getData();
                                    //String convertgambar = UUID.randomUUID()+pictureData.toString();
                                    //String convertgambar = pictureData.toString();
                                   // BufferedImage image = ImageIO.read(new ByteArrayInputStream(pictureData));
                                    //ImageIO.write(image, "jpg", new File("E:\\uploading-files\\src\\main\\resources\\static\\images\\gambar"+" - " + ".jpg"));
//                                    System.out.println("picture: " + pictureData);
//                                    System.out.println(teksaja);
                                    //String gambar = image + ".jpg";

//                                    String imgPath = "E:\\____________\\________________________\\tmp\\"
//                                            + UUID.randomUUID().toString().replaceAll("-", "") + ".jpg";
//                                    File imgFile = new File(imgPath);
//                                    FileOutputStream out = new FileOutputStream(imgFile);
//                                    out.write(pictureData);
//                                    out.close();

                                    String encodeimage = Base64.encodeBase64String(pictureData);

                                    soal.setGambar(pictureData);
                                    soal.setBase64encodedimage(encodeimage);
                                    //soal.setGambar(pictureData);

                                    soalDao.save(soal);
                                    //System.out.println(s);
                                }
                            }
                        }
                    }
                }
            }
        }

//        for ( XWPFTable table : xdoc.getTables()) {
//            String matauji = table.getRow(0).getCell(1).getText();
//            String penulis = table.getRow(0).getCell(3).getText();
//            String no = table.getRow(1).getCell(1).getText();
//            String paket = table.getRow(1).getCell(3).getText();
//            String kompetensi = table.getRow(2).getCell(1).getText();
//            String indikator = table.getRow(3).getCell(1).getText();
//            String materi = table.getRow(4).getCell(1).getText();
//            String spesifikasi = table.getRow(5).getCell(1).getText();
//            String level = table.getRow(6).getCell(1).getText();
//            String teks = table.getRow(7).getCell(1).getText();
//            String a = table.getRow(8).getCell(1).getText();
//            String b = table.getRow(9).getCell(1).getText();
//            String c = table.getRow(10).getCell(1).getText();
//            String d = table.getRow(11).getCell(1).getText();
//            String kunci = table.getRow(12).getCell(1).getText();
//            String pembahasan = table.getRow(13).getCell(1).getText();
//
//            Soal soal;
//            soal = new Soal();
//
//            soal.setMatauji(matauji);
//            soal.setPenulis(penulis);
//            soal.setNo(no);
//            soal.setPaket(paket);
//            soal.setKompetensi(kompetensi);
//            soal.setIndikator(indikator);
//            soal.setMateri(materi);
//            soal.setSpesifikasi(spesifikasi);
//            soal.setLevel(level);
//            soal.setText(teks);
//            soal.setA(a);
//            soal.setB(b);
//            soal.setC(c);
//            soal.setD(d);
//            soal.setKunci(kunci);
//            soal.setPembahasan(pembahasan);
//
//
//            soalDao.save(soal);
//            }
//        }

        }
    }


