package com.example.uploadingfiles.service;

import com.example.uploadingfiles.dao.SoalDao;
import com.example.uploadingfiles.dto.ProsesImportDto;
import com.example.uploadingfiles.entity.Soal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class UploadService {

    @Autowired
    private WordGeneratorService wordGeneratorService;

    @Autowired
    private SoalDao soalDao;

    public Page<Soal>getAllAndPage(Pageable page) { return soalDao.findAllByOrderByMatauji(page); }

    public Page<Soal>getByMateriAndPage(String name, Pageable page){
        return soalDao.findAllByMateriContainingIgnoreCaseOrderByNoAsc(name, page);
    }

    public Optional<Soal> getById(String id){
        return soalDao.findById(id);
    }

    public void delete(String id) {
        soalDao.deleteById(id);
    }

    public Runnable importFromWord(MultipartFile file, ProsesImportDto prosesImportDto) {
        return () -> {
            try {
                wordGeneratorService.importUpload(file, prosesImportDto);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        };
    }
}
