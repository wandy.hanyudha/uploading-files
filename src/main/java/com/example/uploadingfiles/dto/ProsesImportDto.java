package com.example.uploadingfiles.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProsesImportDto {
    private int progress;
    private double percentProgress;
    private String status;
    private int totalSize;
    private List<String>  logs;
}
