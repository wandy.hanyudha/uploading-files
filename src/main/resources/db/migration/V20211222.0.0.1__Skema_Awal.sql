create table public.soal
(
    id         varchar(36) not null,
    matauji     varchar(50) not null,
    penulis     varchar(50) not null,
    no          varchar(20),
    paket       varchar(20),
    kompetensi  varchar(50),
    indikator   varchar(50),
    materi      varchar(50),
    spesifikasi varchar(50),
    level       varchar(20),
    text        varchar(500),
    gambar      bytea,
    base64encodedimage  text,
    a           varchar(100),
    b           varchar(100),
    c           varchar(100),
    d           varchar(100),
    kunci       varchar(100),
    pembahasan  varchar(500),
    primary key (id)
);